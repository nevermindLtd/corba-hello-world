# Jérémy MAISSE #

HelloWord CORBA de Mr Jean michel SIMONET

## [JAVA DOC](https://docs.oracle.com/javase/7/docs/api/) ##

### Récupérer le code ###

* Cloner ce répertoire à partir de son [URL](https://bitbucket.org/nevermindLtd/corba-hello-world) dans le répertoire de votre choix sur votre ordinateur :
	
```
cd $HOME/Documents
git clone https://nevermindLtd@bitbucket.org/nevermindLtd/corba-hello-world.git
```
