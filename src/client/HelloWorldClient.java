package client;

import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import HelloWorldApp.HelloWorld;
import HelloWorldApp.HelloWorldHelper;

public class HelloWorldClient {

	public static void main(String[] args) {
		//changer l'adresse IP par defaut 
		//Properties prop = new Properties();
		//prop.put("com.sun.CORBA.ORBServerPort", "3696");
		//prop.put("com.sun.CORBA.ORBServerHost", "198.70.12.1");
		
		// Initialiser l'ORB
		org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args,null);
		try {
			/*
			// Recuperer la reference de l'objet
			java.io.FileInputStream file = new java.io.FileInputStream("ObjectID");
			java.io.InputStreamReader input = new java.io.InputStreamReader(file);
			java.io.BufferedReader reader = new java.io.BufferedReader(input);
			String refStringCorbaObject = reader.readLine();
			file.close();
			
			// Convretir la reference 
			org.omg.CORBA.Object obj = orb.string_to_object(refStringCorbaObject);
			HelloWorld hello = HelloWorldHelper.narrow(obj);
			*/
			// Récupérer le service de nommage
			org.omg.CORBA.Object objref = orb.resolve_initial_references("NameService");
			NamingContextExt refNamingContext = NamingContextExtHelper.narrow(objref);
			
			// Récupérer la référence du Servant
			String name = "Hello";
			
			// Convertir la reférence du Servant
			HelloWorld hello = HelloWorldHelper.narrow(refNamingContext.resolve_str(name));
			
			// Utiliser l'objet
			System.out.println(hello.sayHello());
			
		} catch (InvalidName e) {
			System.out.println("Invalid Name");
			e.printStackTrace();
		} catch (NotFound e) {
			System.out.println("Not Found");
			e.printStackTrace();
		} catch (CannotProceed e) {
			System.out.println("Cannot Proceed");
			e.printStackTrace();
		} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			System.out.println("Invalid Name Naming Context");
			e.printStackTrace();
		}
		
	}
	
}
