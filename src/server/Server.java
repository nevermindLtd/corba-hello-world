package server;

import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import HelloWorldApp.HelloWorld;
import HelloWorldApp.HelloWorldHelper;

public class Server {

	public static void main(String[] args) {
		org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args,null);
		POA rootpoa; // Portable Object Adapter
		try {
			// Initialiser le gestionnaire de Servant
			rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			// Initialiser le gestionnaire de POA
			rootpoa.the_POAManager().activate();
			
			// Instancier le Servant
			HelloWorldServant helloWorldServant = new HelloWorldServant();
			// Recupérer la référence d'objet corba du Servant
			org.omg.CORBA.Object refCorbaObject = rootpoa.servant_to_reference(helloWorldServant);
			HelloWorld refHelloWorldServant = HelloWorldHelper.narrow(refCorbaObject);
			
			// Recuperer le service de nommage
			org.omg.CORBA.Object refNameService = orb.resolve_initial_references("NameService");
			NamingContextExt refNameingContext = NamingContextExtHelper.narrow(refNameService);
			
			// Lier la reférence de l'objet au service de nommage
			String name = "Hello";
			NameComponent path[] = refNameingContext.to_name(name);
			refNameingContext.rebind(path, refHelloWorldServant);
			
			// Lancer le server
			System.out.println("Server lunch !");
			orb.run();
		} catch (InvalidName e) {
			System.out.println("Invalid Name");
			e.printStackTrace();
		} catch (AdapterInactive e) {
			System.out.println("Adapter Inactive");
			e.printStackTrace();
		} catch (ServantNotActive e) {
			System.out.println("Servant Not Active");
			e.printStackTrace();
		} catch (WrongPolicy e) {
			System.out.println("Wrong Policy");
			e.printStackTrace();
		} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			System.out.println("Invalid Name while binding Servant to Naming Context");
			e.printStackTrace();
		} catch (NotFound e) {
			System.out.println("Servant Not Found while binding to Naming Context");
			e.printStackTrace();
		} catch (CannotProceed e) {
			System.out.println("Cannot Proceed while binding to Naming Context");
			e.printStackTrace();
		}
	}

}
